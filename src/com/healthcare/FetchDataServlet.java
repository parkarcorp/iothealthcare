package com.healthcare;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.healthcare.datamodels.Patient;
import com.healthcare.models.PMFHttpServlet;
import com.healthcare.models.ServerResponse;

@SuppressWarnings("serial")
public class FetchDataServlet extends PMFHttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		ServerResponse response;

		List<Patient> requestedTasks = getPersistentObjects(
				Patient.class);

		System.out.println("Tasks : " + requestedTasks);

		if (requestedTasks == null || requestedTasks.size() <= 0) {
			response = new ServerResponse(1,
					"No Patient Data is available",
					null);
		} else {
			Patient[] modelArray = requestedTasks
					.toArray(new Patient[requestedTasks.size()]);
			response = new ServerResponse(0,
					null,
					new Gson().toJson(modelArray, Patient[].class));
		}

		resp.getWriter().write(new Gson().toJson(response));
	}
}
