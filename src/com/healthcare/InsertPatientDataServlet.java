package com.healthcare;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.healthcare.datamodels.Patient;
import com.healthcare.models.PMFHttpServlet;
import com.healthcare.models.ServerResponse;
import com.healthcare.utils.StringUtils;

@SuppressWarnings("serial")
public class InsertPatientDataServlet extends PMFHttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String jsonRequest = getRequestString(req);

		ServerResponse response;
		if (!StringUtils.isNullOrEmpty(jsonRequest)) {
			Patient requestData = new Gson().fromJson(jsonRequest,
					Patient.class);

			System.out.println("request : " + requestData);
			try {

				if (doPersistent(requestData)) {
					response = new ServerResponse(0, "Patient data added",
							null);
				} else {
					response = new ServerResponse(1,
							"Failed to add patient data",
							null);

				}

			} catch (Exception e) {
				response = new ServerResponse(1, "Failed to add patient data",
						null);
			}

		} else {
			response = new ServerResponse(1, "Invalid Request", null);
		}

		resp.getWriter().write(new Gson().toJson(response));
	}

}
