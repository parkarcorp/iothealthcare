package com.healthcare;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.healthcare.datamodels.LoginData;
import com.healthcare.models.PMFHttpServlet;
import com.healthcare.models.ServerResponse;
import com.healthcare.utils.StringUtils;

public class LoginServlet extends PMFHttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String jsonRequest = getRequestString(req);

		ServerResponse response;
		boolean validUser = false;
		if (!StringUtils.isNullOrEmpty(jsonRequest)) {
			LoginData requestData = new Gson().fromJson(jsonRequest,
					LoginData.class);
			System.out.println("request : " + requestData);

			try {
				LoginData savedData = getPersistentObject(LoginData.class,
						requestData.getUserName());
				System.out.println("savedData : " + savedData);

				if (savedData != null
						&& savedData.getPassword().equals(
								requestData.getPassword())) {
					validUser = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (validUser) {
			response = new ServerResponse(0, "SUCCESS", "Login Successfull");
		} else {

			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

			response = new ServerResponse(1, "Fail",
					"Invalid username/password");
		}

		resp.getWriter().write(new Gson().toJson(response));
	}

}