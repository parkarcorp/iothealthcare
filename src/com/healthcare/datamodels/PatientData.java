package com.healthcare.datamodels;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable
public class PatientData implements Comparable<PatientData> {

	@Persistent(mappedBy = "patientData")
	private Patient parentPatient;

	@Persistent
	private String logDate;

	@Persistent
	private String force;

	@Persistent
	private String heartBeat;

	public PatientData(String force, String heartBeat) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				Locale.ENGLISH);
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		long currentTime = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		cal.setTimeInMillis(currentTime);

		this.logDate = dateFormat.format(cal.getTime());

		this.force = force;
		this.heartBeat = heartBeat;
	}

	public String getLogDate() {
		return logDate;
	}

	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}

	public String getForce() {
		return force;
	}

	public void setForce(String force) {
		this.force = force;
	}

	public String getHeartBeat() {
		return heartBeat;
	}

	public void setHeartBeat(String heartBeat) {
		this.heartBeat = heartBeat;
	}

	@Override
	public String toString() {
		return "PatientData [logDate=" + logDate + ", force=" + force
				+ ", heartBeat=" + heartBeat + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((logDate == null) ? 0 : logDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PatientData other = (PatientData) obj;
		if (logDate == null) {
			if (other.logDate != null)
				return false;
		} else if (!logDate.equals(other.logDate))
			return false;
		return true;
	}

	@Override
	public int compareTo(PatientData o) {
		return this.logDate.compareTo(o.logDate);
	}

}
