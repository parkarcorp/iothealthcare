package com.healthcare;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.healthcare.datamodels.LoginData;
import com.healthcare.models.PMFHttpServlet;
import com.healthcare.models.ServerResponse;
import com.healthcare.utils.StringUtils;

public class AddUserServlet extends PMFHttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String jsonRequest = getRequestString(req);

		ServerResponse response;
		if (!StringUtils.isNullOrEmpty(jsonRequest)) {
			LoginData requestData = new Gson().fromJson(jsonRequest,
					LoginData.class);
			System.out.println("request : " + requestData);
			try {

				if (doPersistent(requestData)) {
					response = new ServerResponse(0, "User Saved", null);
				} else {
					response = new ServerResponse(1,
							"Failed to Save User",
							null);
				}
			} catch (Exception e) {
				response = new ServerResponse(1, "Failed to Save User",
						null);
			}

		} else {
			response = new ServerResponse(1, "Invalid Request", null);
		}

		resp.getWriter().write(new Gson().toJson(response));
	}

}